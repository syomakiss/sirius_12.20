import setuptools
from setuptools import find_packages, setup

setup(
	name='SiriusCXRSeg',
	version="0.0.2",
    author="Sirius_nauka16",
    author_email="dkhasanov76@gmail.com",
   	description="Проект по сегментации легких.",
    packages=find_packages(),
	package_data = {
		'SiriusCXRSeg': [
		    'trained_models/Model_all_parts_v2',
		    ]
    	},
	classifiers=[
		"Programming Language :: Python :: 3",
		"License :: OSI Approved :: Apache Software License",
		"Operating System :: OS Independent",
    	],
	install_requires=[
		"numpy==1.19.4",
		"torchvision==0.8.1",
		"torch==1.7.0",
		"opencv-python==4.1.2.30",
		"matplotlib==3.2.2",
		"imageio==2.4.1",
		"albumentations==0.5.2",
		"imgaug==0.4.0",
		"segmentation-models-pytorch==0.1.3",
		"sklearn==0.0",
		"pathlib==1.0.1",
		"segmentation_models_pytorch==0.1.3",
		"keras_retinanet==1.0.0",
		"streamlit==0.73.1",
        ],
	python_requires='>=3.6.9',
)

