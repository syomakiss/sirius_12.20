import numpy as np
import torch
from torch.utils.data import Dataset, DataLoader
import cv2
import imageio
import pathlib
from pathlib import Path


class LungSegmentationDataset(Dataset):
    def __init__(self, image_list, path, parts_of_body=('heart', 'left clavicle', 'left lung', 'right clavicle', 'right lung'), transformations=None):
        if type(path) == pathlib.PosixPath:
            self.path = path
        else:
            self.path = Path(path)
        self.images = image_list
        self.transformations = transformations
        self.parts_of_body = parts_of_body

    def __len__(self):
        return len(self.images)

    def __getitem__(self, index):
        filename = self.images[index]
        image = cv2.imread(str(self.path / 'Images' / filename), cv2.IMREAD_COLOR)
        mask_name = filename.split(".")[0] + ".gif"
        result = {'image': image, 'right clavicle': None, 'left clavicle': None, 'left lung': None, 'right lung': None,
                  'heart': None}

        for part in self.parts_of_body:
            img = imageio.imread(str(self.path / 'scratch' / 'masks' / part / mask_name))
            if len(img.shape) == 2:
                img = np.repeat(img[..., None], 3, -1)
            result[part] = img

        result = {key: value for key, value in result.items() if value is not None}

        if self.transformations:
            result = self.transformations(**result)
        to_stack = []
        for key, value in result.items():
            if key != 'image':
                mask = value[0] if isinstance(value, torch.Tensor) else torch.tensor(value[..., 0])
                mask -= mask.min()
                mask = mask / mask.max()
                mask = torch.round(mask)
                to_stack.append(mask)

        result['image'] -= result['image'].min()
        result['image'] = result['image'] / result['image'].max()

        return result['image'], torch.stack(to_stack)

def get_url_to_download_dataset():
    print('https://drive.google.com/drive/folders/1BacUZSKZMffMchhyWuKnorKeWzD_MdRW?usp=sharing')