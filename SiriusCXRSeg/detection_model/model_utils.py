from keras_retinanet import models
from keras_retinanet.utils.image import read_image_bgr, preprocess_image, resize_image
from keras_retinanet.utils.visualization import draw_box

import os
import cv2
import numpy as np
from nms_soft import nms_standard


nms_thresh = 0.1
image_min_side = 512
color = [0, 0, 255]
conf_thresh = 0.19


def load_pneumonia_model():
    model = models.load_model('/var/www/markovav.domains/sirius_12.20/SiriusCXRSeg/trained_models/pneumonia_detection_resnet50_csv_11.h5', backbone_name='resnet50')
    return model


def predict_pneumonia(model, file_path, output_path):
    image = read_image_bgr(file_path)
    draw = image.copy()

    image = preprocess_image(image)
    image, scale = resize_image(image, min_side=image_min_side)
    boxes, scores, labels = model.predict_on_batch(np.expand_dims(image, axis=0))
    boxes /= scale

    # prepare data for nms
    scores = np.squeeze(scores)
    scores = np.expand_dims(scores, axis=1)
    boxes = np.squeeze(boxes)
    boxes_nms = np.concatenate((scores, boxes), axis=1)
    nms_labels = nms_standard(boxes_nms, nms_thresh)

    # prepare data for drawing boxes
    boxes = boxes[nms_labels]
    scores = scores[nms_labels]
    boxes = np.expand_dims(boxes, axis=0)
    scores = scores.reshape((1, scores.shape[0]))
    if np.max(scores) >= conf_thresh:
        # draw boxes
        for box, score, label in zip(boxes[0], scores[0], labels[0]):
            if score < conf_thresh:
                continue
            b = box.astype(int)
            draw_box(draw, b, color=color)

        inf_score = np.min([0.95, (scores.max() / conf_thresh * 0.5)])  # set confidence level
        cv2.imwrite(output_path, draw)

    else:
        inf_score = np.min([0.95, (scores.max() / conf_thresh * 0.5)])
        cv2.imwrite(output_path, draw)

    binary_label = int(np.max(scores) > conf_thresh)
    return inf_score * 100, binary_label

def predict_pneumonia_with_boxes(model, file_path, output_path):
    image = read_image_bgr(file_path)
    draw = image.copy()

    image = preprocess_image(image)
    image, scale = resize_image(image, min_side=image_min_side)
    boxes, scores, labels = model.predict_on_batch(np.expand_dims(image, axis=0))
    boxes /= scale

    # prepare data for nms
    scores = np.squeeze(scores)
    scores = np.expand_dims(scores, axis=1)
    boxes = np.squeeze(boxes)
    boxes_nms = np.concatenate((scores, boxes), axis=1)
    nms_labels = nms_standard(boxes_nms, nms_thresh)

    # prepare data for drawing boxes
    boxes = boxes[nms_labels]
    scores = scores[nms_labels]
    boxes = np.expand_dims(boxes, axis=0)
    scores = scores.reshape((1, scores.shape[0]))

    return_boxes = []
    if np.max(scores) >= conf_thresh:
        # draw boxes
        for box, score, label in zip(boxes[0], scores[0], labels[0]):
            if score < conf_thresh:
                continue
            b = box.astype(int)
            return_boxes.append(b)
            draw_box(draw, b, color=color)

        inf_score = np.min([0.95, (scores.max() / conf_thresh * 0.5)])  # set confidence level
        cv2.imwrite(output_path, draw)

    else:
        inf_score = np.min([0.95, (scores.max() / conf_thresh * 0.5)])
        cv2.imwrite(output_path, draw)

    binary_label = int(np.max(scores) > conf_thresh)
    return inf_score * 100, binary_label, return_boxes
