import os
import sys
import segmentation_models_pytorch as smp
os.environ['KMP_DUPLICATE_LIB_OK']='True'
sys.path.append(os.path.join(os.path.dirname(os.path.abspath(__file__)), os.pardir, 'model'))
sys.path.append(os.path.join(os.path.dirname(os.path.abspath(__file__)), os.pardir, 'detection_model'))

import cv2
import torch
import model_utils
import model_wrapper
import numpy as np
import streamlit as st
from albumentations import Compose
from albumentations.augmentations.transforms import Resize
from imgaug.augmentables.segmaps import SegmentationMapsOnImage


organs = ['L lung 1', 'L lung 2', 'L lung 3', 'L lung 4', 'R lung 1', 'R lung 2', 'R lung 3', 'R lung 4', 'L clavicle', 'R clavicle', 'Heart']

organs_colors = {
    'L lung 1': 3,
    'L lung 2': 4,
    'L lung 3': 5,
    'L lung 4': 6,
    'R lung 1': 3,
    'R lung 2': 4,
    'R lung 3': 5,
    'R lung 4': 6,
    'L clavicle': 1,
    'R clavicle': 1,
    'Heart': 1
}


@st.cache
def contour_to_mask(shape, x1y1, x2y2):
  answer = np.zeros(shape)
  for i in range(len(answer)):
    for j in range(len(answer[i])):
      if (i >= x1y1[1] and i <= x2y2[1]) and (j >= x1y1[0] and j <= x2y2[0]):
        answer[i][j] = 1
  return answer


@st.cache
def IoU(prediction_mask, true_mask):
  intersection = (prediction_mask == 1) & (true_mask == 1)
  union = (prediction_mask == 1) | (true_mask == 1)
  iou = np.sum(intersection) / np.sum(union)
  return iou


def predict_pneumonia(img: np.ndarray):
  model = model_utils.load_pneumonia_model()
  cv2.imwrite('test.png', img)
  _, _, boxes = model_utils.predict_pneumonia_with_boxes(model, 'test.png', 'test.png')
  return boxes, cv2.imread('test.png')


def get_models() -> list:
    models = os.listdir(os.path.join(os.path.dirname(os.path.abspath(__file__)), os.pardir, 'trained_models'))
    for model in models.copy():
        if '.' in model: models.remove(model)
    return models


@st.cache
def predict(img: np.ndarray, model_name: str) -> dict:
    model_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), os.pardir, 'trained_models', model_name)
    model = torch.load(model_path, map_location=torch.device('cpu'))
    masks = model_wrapper.return_masks(model, img.astype('uint8'), device=torch.device('cpu'))
    left_lung = masks.pop('L lung')
    left_segments = mask_lung_segments(left_lung)
    masks['L lung 1'] = left_segments[0]
    masks['L lung 2'] = left_segments[1]
    masks['L lung 3'] = left_segments[2]
    masks['L lung 4'] = left_segments[3]
    right_lung = masks.pop('R lung')
    right_segments = mask_lung_segments(right_lung)
    masks['R lung 1'] = right_segments[0]
    masks['R lung 2'] = right_segments[1]
    masks['R lung 3'] = right_segments[2]
    masks['R lung 4'] = right_segments[3]
    boxes, bbox_image = predict_pneumonia(img)
    pneumonia_organs = []
    for box in boxes:
        for organ, mask in masks.items():
            mask = cv2.resize(mask.astype('uint8'), (img.shape[1], img.shape[0]))
            if IoU(mask, contour_to_mask((img.shape[0], img.shape[1]), (box[0], box[1]), (box[2], box[3]))) > 0.05 and organ not in pneumonia_organs:
                pneumonia_organs.append(organ)
    return masks, pneumonia_organs, bbox_image


@st.cache
def concat_masks(masks: dict, contour: bool) -> np.ndarray:
  if (len(masks.keys()) == 0): return None
  shape = masks[list(masks.keys())[0]].shape
  result = np.zeros(shape)
  for organ in organs:
      if organ in masks.keys():
          cur_mask = one_mask_to_contour(masks[organ].astype('uint8')) if contour else masks[organ]
          parsed = (cur_mask > 0)*organs_colors[organ]
          result[cur_mask > 0] = parsed[cur_mask > 0]
  return result


@st.cache
def apply_mask_on_image(img: np.ndarray, mask: np.ndarray) -> np.ndarray:
  if mask is None: return img
  start_image = Compose([Resize(*(mask.shape))])(image=img)['image']
  segmap = SegmentationMapsOnImage(mask.astype('uint8'), shape=start_image.shape)
  image_to_show = cv2.resize(segmap.draw_on_image(start_image)[0], (img.shape[1], img.shape[0])) 
  return image_to_show


@st.cache
def one_mask_to_contour(mask: np.ndarray) -> np.ndarray:
  new_mask = np.zeros(mask.shape, dtype=np.uint8)
  ct, _ = cv2.findContours(mask, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
  cv2.drawContours(new_mask, ct, -1, 255, thickness=2)
  return new_mask


@st.cache
def mask_lung_segments(mask):
  up_line = 0
  down_line = 0
  for line in range(len(mask)):
    if mask[line].sum() > 0:
      up_line = line
      break
  for line in range(len(mask) - 1, 0, -1):
    if mask[line].sum() > 0:
      down_line = line
      break
  height = (down_line - up_line) // (3.5)
  up_line, down_line = int(up_line + height), int(down_line - height)
  zeros = np.zeros_like(mask)
  zeros[up_line:(down_line + 1), :] = zeros[up_line:(down_line + 1), :] + 2
  new_mask = zeros + mask
  start_st = 0
  finish_st = 0
  for st in range(len(new_mask[0])):
    if 3 in new_mask[:, st]:
      start_st = st
  for st in range(len(new_mask[0]) - 1, 0, -1):
    if 3 in new_mask[:, st]:
      finish_st = st
  medium = (start_st + finish_st) // 2
  answer1 = np.zeros_like(mask)
  answer2 = np.zeros_like(mask)
  answer3 = np.zeros_like(mask)
  answer4 = np.zeros_like(mask)
  for line in range(len(mask)):
    for st in range(len(mask[line])):
      if new_mask[line][st] == 0:
        pass
      elif new_mask[line][st] == 1 and line <= up_line:
        answer1[line][st] = 1
      elif new_mask[line][st] == 3 and st < medium:
        answer2[line][st] = 1
      elif new_mask[line][st] == 3 and st >= medium:
        answer3[line][st] = 1
      elif new_mask[line][st] == 1 and line >= down_line:
        answer4[line][st] = 1
  return answer1, answer2, answer3, answer4


@st.cache
def get_min_max_X(mask):
  min_x = 1000
  max_x = -1
  for i in range(len(mask)):
    for j in range(len(mask[0])):
      if mask[i][j] > 0:
        if min_x > j: min_x = j
        if max_x < j: max_x = j
  return min_x, max_x