"""
## CXRsegmentation

DESCRIPTION

Author: [SIRIUS INNO TEAM](https://URL_TO_YOU))\n
Source: [Github](https://github.com/URL_TO_CODE)
"""

import segmentation
import userdata
import morph

from PIL import Image
import streamlit as st
import numpy as np


@st.cache
def load_image(image_file, bw):
    img = np.array(Image.open(image_file).convert('L')) if bw else np.array(Image.open(image_file)).astype('uint8')
    if not bw and len(img.shape) != 3:
        img = np.repeat(img[..., None], 3, -1)
    return img


def main():
    # HEADER
    st.set_page_config(layout='wide')
    st.title("CXRsegmentation")
    
    # FILE UPLOAD
    image_file = st.file_uploader("Upload Image",type=['png','jpeg','jpg','bmp'])
    if image_file is not None:  
        # DATA LOAD
        glob = userdata.get_session(image_file.id)

        # ORIGINAL IMAGE VIEW
        if glob['image'] is None:
            glob['uploaded_file'] = load_image(image_file, False)
            glob['image'] = load_image(image_file, True)
            glob['predict_image'] = None
            glob['bbox_image'] = None
        
        col1, col2 = st.beta_columns(2)

        # MAIN
        invert = st.sidebar.checkbox('Invert image')
        #selected_model = st.sidebar.selectbox('Select model', segmentation.get_models())
        selected_masks = st.sidebar.multiselect('Select organs', segmentation.organs, default=segmentation.organs)
        button_predict, button_reset = st.sidebar.button('Predict'), st.sidebar.button('Reset')
        if glob['predict_image'] is None or button_reset or np.allclose(glob['predict_image'], glob['uploaded_file']) or np.allclose(np.invert(glob['predict_image']), glob['uploaded_file']):
            glob['predict_image'] = np.invert(glob['uploaded_file']) if invert else glob['uploaded_file']
            if button_reset: 
                glob['bbox_image'] = None
                glob['prediction_runned'] = False
        if button_predict:
            with st.spinner(text='Segmentation in progress'):
                filtered_masks = {}
                image_to_predict = np.invert(glob['uploaded_file']) if invert else glob['uploaded_file']
                predict, pneumonia_organs, bbox_image = segmentation.predict(image_to_predict, 'Model_all_parts_v2')
                glob['bbox_image'] = bbox_image if len(pneumonia_organs) != 0 else None
                glob['prediction_runned'] = True
                lung_size = segmentation.get_min_max_X(predict['L lung 4'])[1]-segmentation.get_min_max_X(predict['R lung 4'])[0]
                heart_size = segmentation.get_min_max_X(predict['Heart'])[1] - segmentation.get_min_max_X(predict['Heart'])[0]
                KTI = heart_size/lung_size
                glob['KTI'] = KTI
                for org in pneumonia_organs.copy():
                    if 'lung' not in org: pneumonia_organs.remove(org)
                glob['pneumonia_organs'] = pneumonia_organs
                for organ in selected_masks: filtered_masks[organ] = predict[organ]*255
                concated_masks = segmentation.concat_masks(filtered_masks, True)
                glob['predict_image'] = segmentation.apply_mask_on_image(image_to_predict, concated_masks).astype('uint8')
        if glob['prediction_runned']:
            with col1:
                if 'bbox_image' in glob and glob['bbox_image'] is not None:
                    st.subheader("Обнаружены лёгочные тени: ")
                    st.image(glob['bbox_image'], width=430, height=430)
                else:
                    st.subheader("Лёгочные тени не обнаружены.")
                    st.image(glob['uploaded_file'], width=430, height=430)
            with col2:
                if glob['prediction_runned']:
                    st.subheader("Сегментированные органы грудной клетки:")
                st.image(glob['predict_image'], width=430, height=430)
            with st.beta_container():
                st.subheader("ПРОТОКОЛ РЕНТГЕНОГРАФИИ ГРУДНОЙ КЛЕТКИ")
                st.subheader("  Вид исследования: Рентгенография органов грудной клетки в прямой проекции.")
                st.subheader("  Доза облучения: 0,03 мЗв.")
                st.subheader("  ПРОТОКОЛ ИССЛЕДОВАНИЯ:")
                if 'bbox_image' in glob and glob['bbox_image'] is not None:
                    st.subheader("  Обнаружены тени в отделах: " + ', '.join(glob['pneumonia_organs']) + ".")
                else:
                    st.subheader("  Лёгочные тени не обнаружены.")
                if glob['KTI'] > 0.5:
                    st.subheader("  Размер сердца составляет {}% от диаметра грудной клетки. Сердце увеличено. Подозрение на кардиомегалию.".format(int(glob['KTI']*100)))
                else:
                    st.subheader("  Размер сердца составляет {}% от диаметра грудной клетки. Размеры сердца в норме.".format(int(glob['KTI']*100)))
        else:
            with col1:
                if glob['prediction_runned']:
                    st.subheader("Сегментированные органы грудной клетки:")
                st.image(glob['predict_image'], width=430, height=430)            
            # st.subheader("")
        # DATA SAVE
        userdata.save_session(image_file.id, glob)


if __name__ == "__main__":
    main()